'''
Created on Jul 16, 2013

@author: Paul
'''
import SocketServer
from socket import error as socketError

players = {}

class Player(SocketServer.BaseRequestHandler):
    
    def handle(self):
        try:
            # everyone has unique name
            self.vars = {'name' : 'anonymous'}
            while players.has_key(self.vars['name']):
                self.vars['name'] = self.vars['name'] + "1"
                
            while True:
                data = self.read()
                
                if len(data) != 0:
                    pID = ord(data[0])
                    msg = data[1:]
                    self.readPacket(pID, msg)
        except socketError:
            self.disconnect()
    
    def disconnect(self):
        players.pop(self.vars['name'])
        self.broadcast(self.vars['name'] + " has disconnected.")
    
    def readPacket(self, pID, msg):
        if pID == 0:    # packet error
            print "packet error"
        elif pID == 1:  # connected with name
            name = msg
            while players.has_key(name):
                name = name + "1"
            self.vars['name'] = name
            players[self.vars['name']] = self
            self.broadcast(self.vars['name'] + " has joined!")
            self.send(self.vars['name'])
        elif pID == 2:  # broadcast message
            if msg.startswith("/"):
                if not " " in msg:
                    cmd = msg
                    arg = ""
                    args = []
                    self.command(cmd, arg, args)
                else:
                    cmd = msg.split(" ", 1)[0]
                    arg = msg.split(" ", 1)[1]
                    args = msg.split(" ")[1:]
                    self.command(cmd, arg, args)
            else:
                self.broadcast(self.vars['name'] + ": " + msg)
        else:
            print "[" + str(pID) + "] unrecognized packet: " + msg
    
    def command(self, cmd, arg, args):
        if cmd == "/me":
            self.broadcast(self.vars['name'] + " " + arg)
        elif cmd == "/rename":
            name = arg.strip()
            while players.has_key(name):
                name = name + "1"
            self.broadcast(self.vars['name'] + " has renamed to " + name + ".")
            players.pop(self.vars['name'])
            self.vars['name'] = name
            self.send("You have renamed to " + name)
            players[self.vars['name']] = self
        else:
            self.send("not a command: " + cmd)
            print "[3] not a command: " + cmd
    
    def read(self):
        return self.request.recv(1024).strip()
    
    def send(self, msg):
        self.request.send(msg)
    
    def broadcast(self, msg):
        for p in players.values():
            if p != self:
                p.send(msg)
        print msg

if __name__ == "__main__":
    print "Setting up..."
    HOST, PORT = "", 9999
    
    print "Starting server..."
    # Create the server, binding to localhost on port 9999
    server = SocketServer.ThreadingTCPServer((HOST, PORT), Player)
    print "Set up!"

    print "Serving..."
    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()


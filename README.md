A simple server and client made in Python. Have someone run Server.py,
then use as many versions of Client.py as you want to have. Communication
should be pretty straightforward after that.
Rudimentary chat client included as an example.

Please use the email daniel@danielseabra.net for any support questions.
from sys import stdout, exit
from time import sleep
from socket import socket, AF_INET, SOCK_STREAM
from socket import error as socketError
from thread import start_new_thread
from thread import error as threadError
from Getch import _Getch
getch = _Getch()

HOST, PORT = "localhost", 9999
RUNME = 500000  # in seconds

# TODO: Replace exit(0) for socketErrors with something more effective.
# Right now they are not exiting because they are not being called from the
# 'main thread'
# See docs: http://docs.python.org/2/library/sys.html#sys.exit


class Client():
    """ A simple chat client """

    def __init__(self, connectionInfo):
        """ Initialize the client with connectionInfo = (HOST, PORT) """

        try:
            stdout.write("Name: ")
            name = raw_input()
            self.sock = socket(AF_INET, SOCK_STREAM)
            self.sock.connect(connectionInfo)
            self.send(1, name)
            self.name = self.read()
            print "HELLO " + self.name + "!"
            print "You are now connected. \n"
        except socketError:
            print "There was an error in connecting to the server. Please " + \
                  "check that the server is actually running!"
            exit(0)
        self.start()

    def start(self):
        """ Start the threads necessary for input and output. """
        self.cin = []
        self.cout = ""
        self.coutQ = ""
        try:
            start_new_thread(self.input, ())
            start_new_thread(self.output, ())
            start_new_thread(self.run, ())
            while True:
                sleep(RUNME)
        except threadError:
            print "There was an error in creating a new thread. This is " + \
                  "probably a problem with your computer."
            exit(0)

    def run(self):
        """ Main loop for client. Keep threads running, capture user input and
        output.
        """

        while True:
            if len(self.cin) != 0:
                for c in self.cout:
                    stdout.write("\b \b")
                for msg in self.cin:
                    stdout.write(msg + "\n")
                stdout.write(self.cout)
                self.cin = []

            if len(self.coutQ) > 0:
                temp = self.coutQ
                self.coutQ = self.coutQ[len(temp):]
                for c in temp:
                    if ord(c) == 8 and len(self.cout) > 0:
                        stdout.write("\b \b")
                        self.cout = self.cout[0:-1]
                    elif ord(c) == 13:
                        stdout.write("\n")
                        self.send(2, self.cout)
                        self.cout = ""
                    else:
                        stdout.write(c)
                        self.cout += c

    def input(self):
        """ Call read() and process information """
        try:
            while True:
                msgIn = self.read()
                self.cin.append(msgIn)
        except socketError:
            print "The server has disconnected. Go beat up your sysadmin."
            exit(0)

    def output(self):
        """ Read user input from termianl (single character) """
        try:
            while True:
                c = getch()
                self.coutQ += c
        except socketError:
            print "The server has disconnected. Go beat up your sysadmin."
            exit(0)

    def read(self):
        """ Read information passed from the server """
        return self.sock.recv(1024).strip()

    def send(self, pID, msg):
        """ Send information to the server """
        self.sock.sendall(chr(pID) + msg)


if __name__ == "__main__":
    Client((HOST, PORT))
